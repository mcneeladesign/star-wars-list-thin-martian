# Star Wars React list
The demo can be viewed [here](https://star-wars-api-d6964.firebaseapp.com/). It is responsive and should work at any size, but ideally around 400px wide looks good. If this where in production I would use media queries and some more flexbox trickery to make it look it's best on all screen sizes.

## Instructions to install locally:
- git clone https://mcneeladesign@bitbucket.org/mcneeladesign/star-wars-list-thin-martian.git
- cd star-wars-list-thin-martian
- run 'npm install'
- run 'npm start'

## Resources used:
- [create-react-app](https://github.com/facebookincubator/create-react-app)
- [node-sass](https://github.com/sass/node-sass)
- [Yoda vector icon](https://www.vecteezy.com/people/36181-yoda-vector)
- [Open Sans and Montserrat](https://fonts.google.com/)
