import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Route,
    Switch
} from 'react-router-dom';
import SingleCharacter from '../SingleCharacter/SingleCharacter';
import Favourites from '../Favourites/Favourites';
import ListView from '../ListView/ListView';
import {Loading} from '../common/Loading/Loading';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      format: 'json',
      currentPageData: {},
      currentCharacter: {},
      currentPage: '1',
      favouritesList: []
    };

    fetch(`//swapi.co/api/people/?format=${this.state.format}&page=${this.state.currentPage}`).then((response) => {
      return response.json();
    }).then((data) => {
      this.setState({currentPageData: data});
      this.setState({loading: false});
    }).catch((err) => {
      // Deal with the error handling better
      console.warn("There was an error retrieving the data.");
      console.log(err);
    });

    this.handleFavourite = this.handleFavourite.bind(this);
  }

  handleFavourite(item) {
    const prevFavouritesList = this.state.favouritesList;
    const newFavouritesList = [...prevFavouritesList, item];
    this.setState({favouritesList: newFavouritesList});
    // Use local storeage here to persist favourites list
  }

  render() {
    return (
      <div className="AppComponent">
        {/* If - loading */}
        {this.state.loading ?
          <Loading />
          :
          <div>
            {/* Else - not loading */}
            <Router>
              <Switch>
                <Route
                  path="/favourites"
                  render={(props) => <Favourites appState={this.state} {...props} />}
                />
                <Route
                  path="/:slug"
                  component={SingleCharacter}
                />
                <Route
                  path="/"
                  render={(props) => <ListView appState={this.state} handleFavourite={this.handleFavourite} {...props} />}
                />
              </Switch>
            </Router>
          </div>
        }
      </div>
    );
  }
}

export default App;
