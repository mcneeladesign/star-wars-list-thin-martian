import React, { Component } from 'react';
import Menu from '../Menu/Menu';
import {ListItem} from '../common/ListItem/ListItem';
import {isObjectEmpty} from '../../functions/isObjectEmpty';

class Favourites extends Component {
  constructor(props) {
    super(props);
    this.handleRouteChange = this.handleRouteChange.bind(this);
  }

  // Handle clicks on the items using a slug to display the correct info
  handleRouteChange(item) {
    const slug = item.name.replace(/\s+/g, '-').toLowerCase();
    this.props.history.push(`/${slug}`, { currentCharacter: item });
  }

  render() {
    let listOfCharacters = '';
    if(!isObjectEmpty(this.props.appState.favouritesList)) {
      listOfCharacters = this.props.appState.favouritesList.map((item, i) => {
        return (
          <div key={i}>
            <ListItem
              character={item}
              handleRouteChange={this.handleRouteChange}
              handleFavourite={this.props.handleFavourite}
              displayAddToFavourites={false}
            />
          </div>
        );
      });
    }

    return (
      <div className="FavouritesComponent">
        <Menu />
        {listOfCharacters}
      </div>
    );
  }
}

export default Favourites;
