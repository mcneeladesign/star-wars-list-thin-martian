import React from 'react';
import Menu from '../Menu/Menu';
import {ListItem} from '../common/ListItem/ListItem';
import {Filter} from '../common/Filter/Filter';
import {isObjectEmpty} from '../../functions/isObjectEmpty';

class ListView extends React.Component {
  constructor(props) {
    super(props);
    this.handleRouteChange = this.handleRouteChange.bind(this);
    this.sortHeightTallest = this.sortHeightTallest.bind(this);
    this.sortHeightShortest = this.sortHeightShortest.bind(this);
  }

  // Handle clicks on the items using a slug to display the correct info
  handleRouteChange(item) {
    const slug = item.name.replace(/\s+/g, '-').toLowerCase();
    this.props.history.push(`/${slug}`, { currentCharacter: item });
  }

  // Sortable methods
  sortHeightShortest(arrayToSort) {
    arrayToSort.sort(function(a, b) {
      return parseFloat(a.height) - parseFloat(b.height);
    })
    this.forceUpdate();
  }
  sortHeightTallest(arrayToSort) {
    arrayToSort.sort(function(a, b) {
      return parseFloat(b.height) - parseFloat(a.height);
    })
    this.forceUpdate();
  }

  render() {
    let listOfCharacters = '';
    if(!isObjectEmpty(this.props.appState.currentPageData)) {
      listOfCharacters = this.props.appState.currentPageData.results.map((item, i) => {
        return (
          <div key={i}>
            <ListItem
              character={item}
              handleRouteChange={this.handleRouteChange}
              handleFavourite={this.props.handleFavourite}
              displayAddToFavourites={true}
            />
          </div>
        );
      });
    }

    return (
      <div className="ListViewComponent">
        <Menu />
        <Filter
          results={this.props.appState.currentPageData.results}
          sortHeightTallest={this.sortHeightTallest}
          sortHeightShortest={this.sortHeightShortest}
        />
        {listOfCharacters}
      </div>
    );
  }
}

export default ListView;
