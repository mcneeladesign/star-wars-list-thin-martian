import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import './Menu.css';

class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = {menuActive: false};
  }

  // Handle menu status
  menuHandler() {
    if(this.state.menuActive === false) {
      this.setState({menuActive: true});
      document.body.style.overflowY = "hidden";
    }
    else {
      this.setState({menuActive: false});
      document.body.style.overflowY = "scroll";
    }
  }

  render() {
    return (
      <div className="MenuComponent">
        <div id="navTrigger" className="nav-trigger">
          <img
            id="menuTrigger"
            onClick={this.menuHandler.bind(this)}
            className="menu-icon"
            src="/images/menu-icon.svg"
            alt="menu icon"
          />
        </div>
        <div className={ this.state.menuActive ? "nav-overlay active" : "nav-overlay"}>
          <img
            id="menuCloseTrigger"
            className="menu-close-icon"
            onClick={this.menuHandler.bind(this)}
            src="/images/menu-close-icon.svg"
            alt="menu close icon"
          />
          <div className="container menu">
            <ul>
              <li>
                <NavLink
                  to="/"
                  exact
                  strict
                  activeClassName="active"
                  onClick={this.menuHandler.bind(this)}
                >
                  Home
                </NavLink>
              </li>
              <li>
                <NavLink
                  to="/favourites"
                  exact
                  strict
                  activeClassName="active"
                  onClick={this.menuHandler.bind(this)}
                >
                  Favourites
                </NavLink>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default Menu;
