import React, { Component } from 'react';
import './SingleCharacter.css';

class SingleCharacter extends Component {
  goBackToList() {
    this.props.history.goBack();
  }

  render() {
    return (
      <div className="SingleCharacter">
        <div className="container">
          <img onClick={() => this.goBackToList()} className="back-btn" src="/images/back-btn.svg" alt="logo" />
          <div>
            <h1>{this.props.location.state.currentCharacter.name}</h1>
            <p>{this.props.location.state.currentCharacter.name} is {this.props.location.state.currentCharacter.height}cm tall, weighing in at {this.props.location.state.currentCharacter.mass}lbs, with {this.props.location.state.currentCharacter.hair_color} hair and {this.props.location.state.currentCharacter.eye_color} eyes.</p>
          </div>
        </div>
      </div>
    );
  }
}

export default SingleCharacter;
