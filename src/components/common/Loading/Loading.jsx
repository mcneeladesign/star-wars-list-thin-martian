import React from 'react';
import './Loading.css';

export const Loading = (props) => {
  return (
    <div className="LoadingContainer">
      <h3>“PATIENCE YOU MUST HAVE my young padawan”</h3>
      <h3>...</h3>
      <img id="float" src="/images/yoda.svg" alt=""/>
    </div>
  );
}
