import React from 'react';
import './ListItem.css';

export const ListItem = (props) => {
  if (props.displayAddToFavourites) {
    return (
      <div className="ListItem">
        <span onClick={() => props.handleRouteChange(props.character)}>{props.character.name} - {props.character.height}cm</span>
        <span
          className="add-to-favourites"
          onClick={() => props.handleFavourite(props.character)}
        >
          +
        </span>
      </div>
    );
  }
  else {
    return (
      <div className="ListItem">
        <span onClick={() => props.handleRouteChange(props.character)}>{props.character.name}</span>
      </div>
    );
  }
}
