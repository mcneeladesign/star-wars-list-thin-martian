import React from 'react';
import './Filter.css';

export const Filter = (props) => {
  return (
    <div className="Filter">
      <h5>
        <span onClick={() => props.sortHeightTallest(props.results)}>Tallest</span>
        <span onClick={() => props.sortHeightShortest(props.results)}>Shortest</span>
      </h5>
    </div>
  );
}
